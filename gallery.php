<!DOCTYPE html>
<html>
<head>
  <title>OUR GALLERY</title>
  <link href="style.css" rel="stylesheet" type="text/css" />
    <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<style type="text/css">
  body{
  background: -webkit-linear-gradient(90deg, rgba(231, 31, 17, .7) 0%, #000000 100% ),url(https://assets.nflxext.com/ffe/siteui/vlv3/2503cca1-63b3-4f4a-939d-8d53dd5b404d/446b08f2-cff8-468b-84f3-2e37fb85f9dc/NG-en-20180709-popsignuptwoweeks-perspective_alpha_website_large.jpg) no-repeat center center fixed; 
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
   background-size: cover;
}

</style>

</head>
<body>
  <div id="carousel-example" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carousel-example" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example" data-slide-to="1"></li>
    <li data-target="#carousel-example" data-slide-to="2"></li>
  </ol>

  <div class="carousel-inner">
    <div class="item active">
      <a href="#"><img src="https://res.cloudinary.com/iamfredrick/image/upload/c_scale,h_600,w_1800/v1531614926/Barks-Redmayne-da35e91.jpg" /></a>
    </div>
    <div class="item">
      <a href="#"><img src="https://res.cloudinary.com/iamfredrick/image/upload/c_fit,h_600,w_1800/v1531613572/Netflix-TV-Movies-Filmed-in-Morocco.jpg" /></a>
    </div>

    <div class="item">
      <a href="#"><img src="https://res.cloudinary.com/iamfredrick/image/upload/c_scale,h_700,w_1800/v1531615312/bright_unit_20874_r-e1492467452572.jpg" /></a>
    </div>


  </div>

  <a class="left carousel-control" href="#carousel-example" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
  </a>
  <a class="right carousel-control" href="#carousel-example" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
  </a>
</div>
  <section id="gallery" class="container">
    <div class="page-header">
      <h1 class="text-center">GALLERY</h1>
    </div>

    <img src="https://studybreaks.com/wp-content/uploads/2018/02/tag-e1519690853279.jpg" class="img-thumbnail">
    <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTDgLBXNJiwL-X701_8WVWeDhflRFi9-xDXmwbJNVwSPdOynq1p" class="img-thumbnail">
    <img src="http://www2.pictures.zimbio.com/mp/OFntD-R9aMix.jpg" class="img-thumbnail">
    
    
  </section>

  <!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</body>
</html>