
            function isEmail(email) {
  
                var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  
                return regex.test(email);
                
            }
            
            $(".btn-01").click(function(e) {
                
                e.preventDefault();

                var errorMessage = "";
                var fieldsMissing = "";

                if ($("#firstname").val() == "") {
                    
                    fieldsMissing += "<br>Firstame";
                    
                }
                
                if ($("#lastname").val() == "") {
                    
                    fieldsMissing += "<br>Lastname";
                    
                }
                
                
                if ($("#email").val() == "") {
                    
                    fieldsMissing += "<br>Email";
                    
                }
                                
                if ($("#password").val() == "") {
                    
                    fieldsMissing += "<br>Password";
                    
                }

                
                if (fieldsMissing != "") {
                    
                    errorMessage += "<p>The following field(s) are missing:" + fieldsMissing;
                    
                }
                
                if (isEmail($("#email").val()) == false) {
                    
                    errorMessage += "<p>Your email address is not valid</p>";
                    
                }
                
                
                if (errorMessage != "") {
                    
                    $("#errorMessage").html(errorMessage);
                    
                } else {
                    
                    $("#successMessage").show();
                    $("#errorMessage").hide();
                    $(".form-01").hide();
                    
                }
                
            });
            
           